/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.List;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.PrintWriter;


public class ChatServer {
    
    ArrayList<PrintWriter> escritores = new ArrayList<PrintWriter>();
    Scanner leitor;
    
    public ChatServer() throws Exception{
    	//System.out.print("server on");
    	ServerSocket server;
        try{
            server = new ServerSocket(5000);
            while(true){
                Socket socket = server.accept();
                new Thread(new EscutaCliente(socket)).start();
                PrintWriter p = new PrintWriter(socket.getOutputStream());
                escritores.add(p);
            }
        } catch (IOException e){
            
        }
        
    }
    
    private void encaminharParaTodos(String texto) {
    	for(PrintWriter w : escritores) {
    		try {
	    		w.println(texto);
	    		w.flush();
    		}catch(Exception e) {}
    	}
    }
    
    private class EscutaCliente implements Runnable{
        
        public EscutaCliente(Socket socket) throws Exception{
        	
            try{
                leitor = new Scanner(socket.getInputStream());
            }catch (Exception e){
            
            }
        }
        
        @Override
        public void run() {
            try{
                String texto;
                while((texto = leitor.nextLine()) != null){
                    System.out.println(texto);
                	encaminharParaTodos(texto);
                }
            }catch(Exception e){
                
            }
            
    }
    }
    public static void main(String[] args) throws Exception{
        new ChatServer();
    }
}
